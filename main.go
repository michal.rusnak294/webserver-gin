package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
)

var (
	version, commit, buildTime string
	versions                   Versions
	censored                   = make(map[string]bool)
	censoredRWM                sync.RWMutex
)

func main() {
	/*http.HandleFunc("/api/v1/version", endpoint1)
	http.HandleFunc("/api/v1/author", endpoint2)
	http.HandleFunc("/api/v1/book", endpoint3)
	http.HandleFunc("/api/v1/consors", endpoint4)*/

	/*httpServer := &http.Server{
		Addr: ":8000",
	}

	// Run server
	go func() {
		if err := httpServer.ListenAndServe(); err != http.ErrServerClosed {
			log.Fatalf("HTTP server ListenAndServe: %v", err)
		}
	}()

	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	r.Run() // listen and serve on 0.0.0.0:8080

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()

	<-ctx.Done()

	//
	gracefullCtx, cancelShutdown := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancelShutdown()

	//
	if err := httpServer.Shutdown(gracefullCtx); err != nil {
		log.Printf("shutdown error: %v\n", err)
		os.Exit(1)
	} else {
		log.Printf("gracefully stopped\n")
	}

	os.Exit(0)
	*/

	router := gin.Default()
	router.GET("/api/v1/version", endpoint1)
	router.GET("/api/v1/author", endpoint2)
	router.GET("/api/v1/book", endpoint3)
	router.POST("/api/v1/consors", endpoint4)

	router.Run("localhost:8080")

}

func endpoint1(c *gin.Context) {
	versions = Versions{version, commit, buildTime}
	c.JSON(http.StatusOK, versions)
}

func endpoint2(c *gin.Context) {
	var key Author
	var name Authorname

	if c.Request.Method != http.MethodGet {
		sendError(c.Writer, http.StatusBadRequest, errors.New("unsupported method. GET method is supported only"))
		return
	}

	isbn := c.Request.URL.Query().Get("book")
	if isbn == "" {
		sendError(c.Writer, http.StatusBadRequest, errors.New("missing URL param book which has to contain isbn of book"))
		return
	}

	url := "https://openlibrary.org/isbn/" + isbn + ".json"

	body, err := sendRequest(url)
	if err != nil {
		sendError(c.Writer, http.StatusInternalServerError, err)
		return
	}

	json.Unmarshal(body, &key)

	result := make([]AuthorResponse, len(key.Authors))
	for index := range key.Authors {

		url := "https://openlibrary.org" + key.Authors[index].Key + ".json"
		body, err = sendRequest(url)
		if err != nil {
			sendError(c.Writer, http.StatusInternalServerError, err)
			return
		}

		err := json.Unmarshal(body, &name)
		if err != nil {
			fmt.Println("error:", err)
			sendError(c.Writer, http.StatusInternalServerError, errors.New("cannot create error"))
		}

		key.Authors[index].Key = strings.ReplaceAll(key.Authors[index].Key, "/authors/", "")

		result[index].AuthorName = name.Name
		result[index].Key = key.Authors[index].Key

	}
	resp, err := json.Marshal(result)
	if err != nil {
		log.Println(err)
		sendError(c.Writer, http.StatusInternalServerError, errors.New("cannot create error"))
	}
	c.Writer.Write(resp)
}

func endpoint3(c *gin.Context) {

	time.Sleep(10 * time.Second)

	authorBooks := Entries{}

	if c.Request.Method != http.MethodGet {
		sendError(c.Writer, http.StatusBadRequest, errors.New("unsupported method. GET method is supported only"))
		return
	}

	author := c.Request.URL.Query().Get("book")
	if author == "" {
		sendError(c.Writer, http.StatusBadRequest, errors.New("missing URL param book which has to contain isbn of book"))
		return
	}

	if isCensored(author) {
		sendError(c.Writer, http.StatusBadRequest, errors.New("author is censored"))
		return
	}

	url := "https://openlibrary.org/authors/" + author + "/works.json?limit=2"

	body, err := sendRequest(url)
	if err != nil {
		sendError(c.Writer, http.StatusInternalServerError, err)
		return
	}

	err = json.Unmarshal(body, &authorBooks)
	if err != nil {
		fmt.Println("error:", err)
		sendError(c.Writer, http.StatusInternalServerError, errors.New("cannot create error"))
	}

	respB := make([]RespBooks, len(authorBooks.Entries))

	for index := range authorBooks.Entries {

		respB[index].Name = authorBooks.Entries[index].Title
		respB[index].Key = authorBooks.Entries[index].Key
		respB[index].Revision = authorBooks.Entries[index].Revision
		respB[index].PublishDate = authorBooks.Entries[index].Created.Value
	}

	resp, err := json.Marshal(respB)
	if err != nil {
		log.Println(err)
		sendError(c.Writer, http.StatusInternalServerError, errors.New("cannot create error"))
	}
	c.Writer.Write(resp)
	//logging(c.Request)
}

func endpoint4(c *gin.Context) {
	if c.Request.Method != http.MethodPost {
		sendError(c.Writer, http.StatusBadRequest, errors.New("unsupported method. POST method is supported only"))
		return
	}

	censors := make([]string, 0)
	err := json.NewDecoder(c.Request.Body).Decode(&censors)
	if err != nil {
		log.Print("error", err)
		c.Writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	addCensored(censors)
}

func addCensored(censors []string) {
	censoredRWM.Lock()

	for _, value := range censors {
		censored[value] = true
	}

	censoredRWM.Unlock()
}

func isCensored(author string) bool {
	censoredRWM.Lock()
	defer censoredRWM.Unlock()
	return censored[author]
}

func sendRequest(url string) ([]byte, error) {
	res, err := http.Get(url)
	if err != nil {
		log.Println(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
	}

	return body, err
}

func sendError(w http.ResponseWriter, httpCode int, err error) {
	buf, locErr := json.Marshal(ErrorResponse{Error: err.Error()})
	if locErr != nil {
		log.Println("origin error", err)
		log.Println("json marshall error", locErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(httpCode)
	w.Write(buf)
}

/*func logging(req *http.Request) {
	fmt.Print("Client IP: ", req.RemoteAddr, "\n", "HTTP Method: ", req.Method, "\n", "URL: ", req.URL, "\n", "Time: ", time.Now(), "\n")
}*/
