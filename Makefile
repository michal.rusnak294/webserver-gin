BINARY_NAME=server

build:
	go build -o ${BINARY_NAME} -ldflags="-X 'main.appVersion=v1.0.0' -X 'main.commit=$(shell git rev-parse HEAD)' -X 'main.buildTime=$(shell date --rfc-3339=seconds)'"

run:
	go build -o ${BINARY_NAME} main.go
	./${BINARY_NAME}

clean:
	go clean
	rm ${BINARY_NAME}